### Examination Rules:

- Use this Ruby On Rails Base Template ( https://choyno@bitbucket.org/choyno/api-only-ror-exam.git )
- Asking for help is not allowed
- Exam Duration is 4 Days

### Steps:

- Read API documentation here ( https://documenter.getpostman.com/view/11854024/TzJuBJ64#da9963c3-2c8e-45a7-a52e-b16be3486e66 )
- Create or Fork frontend application here ( https://bitbucket.org/choyno/fe-guide-for-api-only-exam/ ) to your repository
- Put your files in the `/api` folder
- Implement requested API endpoints
- Run your api server in http://localhost:8000
- Run the FE server using `npm i` and `npm start`. It will automatically open in http://localhost:3000
- Push your changes to your repository
- Submit pull request to the main repository master

### Scoring:
### Scoring is based on:
- Completeness of the specs
- Gem used ( listed in the read.me of api-only-ror-exam repo)
- Clearing of the Rubocop and Rails Best Practice Error
- Adding Rspec for every API Request

### Bonus:
- Time spent
- Migration
- Development Env
- Readme
- Git knowledge

